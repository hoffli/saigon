---
layout: blog
title: Book Collection
image: /images/blog/mystery.png
---

**Solaris, by Stanislaw Lem**

> [[PDF] German](http://www.bubblebath.de/BUBBLE_home/Olliwood/Stanislaw_Lem_-_Solaris__German_.pdf)

> Das war ein altes Buch, diese »Einführung in die Solaristik« von Muntius; ich erinnerte mich noch an die Nacht, die ich darüber verbracht hatte, und an Gibarians Lächeln, als er mir dieses Exemplar, sein eigenes, mitgegeben hatte, und an das irdische Morgengrauen im Fenster, als ich bis zu dem Wort »Ende« gelangt war. - Die Solaristik - schreibt Muntius - ist die Ersatzreligion des Weltraumzeitalters, sie ist Glaube, eingehüllt in das Gewand der Wissenschaft; der Kontakt, das Ziel, dem sie entgegenstrebt, ist ebenso nebelhaft und dunkel wie die Gemeinschaft der Heiligen oder die Herabkunft des Messias. Die Erkundung kommt einem in methodologischen Formeln existierenden Liturgie-System gleich; die demütige Arbeit der Forscher ist das Warten auf Erfüllung, auf die Verkündigung, denn Brücken zwischen Solaris und Erde gibt es nicht und kann es nicht geben. Aber diese Selbstverständlichkeit wird wie andere, wie das Fehlen gemeinsamer Erfahrungen, wie das Fehlen von Begriffen, die man übermitteln könnte, von den Solaristen zurückgewiesen, so ähnlich, wie von den Gläubigen die Argumente zurückgewiesen wurden, die ihres Glaubens Grundlage umstürzten. Im übrigen, was erwarten sich Menschen, was können Menschen sich vom »Anknüpfen einer Nachrichtenverbindung« mit denkenden Meeren versprechen?

> [...] die Zeit der grausamen Wunder sei noch nicht um.

**The Antichrist, by Friedrich Nietzsche**

> [[Read online] English](http://ebooks.adelaide.edu.au/n/nietzsche/friedrich/antichrist/)

> In me Christianity ... devours itself.

**Nervous Breakdown, from Schoolmistress and other stories, by Anton Pavlovich Chekhov**

> [[All books by Chekhov], Project Gutenberg](http://www.gutenberg.org/ebooks/author/708)

> "We human beings do murder each other," said the medical student. "It's immoral, of course, but philosophizing doesn't help it. Good-by!"

> [E]verything that is called human dignity, personal rights, the Divine image and semblance, were defiled to their very foundations—"to the very marrow," **as drunkards say**.

> When he came out from the doctor's he was beginning to feel ashamed; the rattle of the carriages no longer irritated him, and the load at his heart grew lighter and lighter as though it were melting away. He had two prescriptions in his hand: one was for bromide, one was for morphia.

> "Come, why look at it? No philosophizing, please. Vodka is given us to be drunk, sturgeon to be eaten, women to be visited, snow to be walked upon. For one evening anyway live like a human being!"


**Man's Search for Meaning, by Viktor Frankl**

> [[PDF] English](http://docdro.id/mYrlhev)

> An abnormal reaction to an abnormal situation is normal behavior.

> Ultimately, man should not ask what the meaning of his life is, but rather must recognize that it is he who is asked.

> Don't aim at success. The more you aim at it and make it a target, the more you are going to miss it. For success, like happiness, cannot be pursued; it must ensue.

**Duše moderního člověka, Carl Gustav Jung**

> [[Read Online] Czech](http://www.teksty.wz.cz/jung-duse_moderniho_cloveka/jung-dmc.html)

> První náznaky zjišťujeme všude kolem sebe: totalitarismus a státní otroctví. Hodnota a význam jedince rapidně klesají a jedinec stále více ztrácí vyhlídky na to, že mu bude dopřáno sluchu.

> [...] jeho infan­tilita a individuální slabost nahrazena člověkem budoucnosti, kte­rý ví, že je sám strůjcem svého osudu a že stát je jeho sluha, nikoli jeho pán.

**Road to Reality, by Roger Penrose**

> [[PDF] English](http://lipn.univ-paris13.fr/~duchamp/Books&more/Penrose/Road_to_Reality-CAPE_JONATHAN_(RAND)(2004).pdf) and much more [in the same directory](http://lipn.univ-paris13.fr/~duchamp/Books&more/Penrose)

> We have a closed circle of consistency here: the laws of physics produce complex systems, and these complex systems lead to consciousness, which then produces mathematics, which can then encode in a succinct and inspiring way the very underlying laws of physics that gave rise to it.

**Borderliners, by Peter Hoeg**

> [[PDF] English](http://docdro.id/akfJgG1)

> The regularity of the clock was a metaphor for the accuracy of the universe. For the accuracy of God's creative achievement. So the clock was, first and foremost, a metaphor.
Like a work of art. And that is how it was. The clock has been like a work of art, a product of the laboratory, a question.
And then, at some point, this has changed. At some point the clock has stopped being a question. Instead it has become the answer.

> The child had wanted attention. She had just asked to be noticed. But she was given an assessment. 'What a clever girl!'

**Goedel, Escher, Bach, by Douglas Hofstadter**

> [[PDF] English](http://www.physixfan.com/wp-content/files/GEBen.pdf)

> I enjoy acronyms. Recursive Acronyms Crablike "RACRECIR" Especially Create Infinite Regress.

**Snowcrash, by Neal Stephenson**

> [[PDF] English](http://hell.pl/agnus/anglistyka/2211/Neal%20Stephenson%20-%20Snow%20Crash.pdf)

> “This Snow Crash thing--is it a virus, a drug, or a religion?”
Juanita shrugs. “What's the difference?”

> “Did you win your sword fight?"<br>
"Of course I won the fucking sword fight," Hiro says. "I'm the greatest sword fighter in the world."<br>
"And you wrote the software."<br>
"Yeah. That, too," Hiro says.

**The Theory of Meaning, by Jakob von Uexküll**

> [[PDF] English](http://docdro.id/w6iN8T1)

> It was a fundamental error on the part of Herbert Spencer to interpret the annihilation of surplus offspring as a “survival of the fittest” in order to predicate progress in the development of living beings on that. It is hardly a matter of the survival of the fittest, but rather, of the survival of the normal in the interests of an unchanging further existence of the species.
