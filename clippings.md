---
layout: blog
title: Clippings
image: /images/blog/mystery.png
---

<style>img, embed, object {width: 100%;}</style>

I will probably add commentary one day.

## **Book Clippings**

![this is how people are talented - stanislaw lem](/images/clippings/talent.png)

![information filtering - stanislaw lem](/images/clippings/gremium.png)

![religion - solaris](/images/clippings/ersatzreligion.jpg)

![religion - antichrist](/images/clippings/christianity.jpg)

![jung  - state](/images/clippings/state.png)

![freshly fallen snow](/images/clippings/crispy.png)

![ivanov  - ethan hawke - remember](/images/clippings/ivanov.png)

_______________________________

## **(Reddit) Troll League**

![cyprus](/images/clippings/cypriots.png)

![cyprus](/images/clippings/cocaine.jpg)

![dylan moran und co](/images/clippings/blackbooks.png)

![taylor vs tayler](/images/clippings/swift.png)

![if yor illegal an y know it clap yo hands](/images/clippings/heathrow.png)

![](/images/clippings/irn.png)

![](/images/clippings/nocontext.jpg)

![](/images/clippings/fomo.jpg)

![](/images/clippings/massive_cock.jpg)
