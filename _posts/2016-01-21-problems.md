---
layout: blog
title: Sudden Problem
category: blog
tags: [read]  
summary: Unexpected problem, half the problem
image: /images/blog/post-pre.jpg
---

_I wrote this in September but I couldn’t post it anywhere before I got safely out of the country. I have been out for some time now so here it is._

_I never expected I could get into this sort of problem but when it was over I found the fact I didn’t expect it made the problem far lighter. It just occurred, I went with it (I guess that was the lack of sleep), it solved itself and the whole situation was over. No invented scenarios, no unnecessary talking, no drama._

____________________

Meet the most legal person on Earth. I mean, in all things immigration.

I am sitting at the headquarters of immigration police in Indonesia.

,,It seems you got the wrong stamp on arrival.” That was two hours earlier in the agency which was supposed to bribe the police to extend my visa. ,,You have to go to the immigration central to fix this first. They will check your documents with the database,” the agency representative looked me straight in the eyes and made a short pause. She knew I am already dead. “Then they will give you a correct stamp. After that we can take care of your visa extension,” she concluded with an assuring smile in a very western European way.

That’s how I wound up here. At the immigration police central. To get a fix because they accidentally put a stamp with wrong date on my fake visa. Yeah, what else should I do, right?

So let’s see what we’ve got here.

Stylish tapestry.

“Entering Indonesia with invalid documents is a serious crime”
“Avoiding immigration control – 1 year in prison”
“Forged travel documents – 5 years in prison”
Two standard office tables with desktop PCs.

Windows XP.

Some shelves with paperwork, very disheveled.

Ordners. “Overstay”, “Passport C”, “Charlie”. Charlie? Like in Viet-Kong?

Smaller shelves on the walls around with old boarding tickets. Other shelves with passports. Hm. Old looking, forgeries? Lost and found? Either way. In a mess like this, no one would ever find out if one disappeared.

CCTV? None?! None in the office. I do a little arm stretch. None in the corridor.

Big windows on two sides of the room. The one to the main hallway has three sets of smudges. As if someone childishly pressed his hands and forehead on it to make someone out notice. Very strange.

And, indeed: Two young policemen. Severely bored, habitually checking their phones. My passport sitting in front of them. We are waiting. The computer with the database of visa documents is down.

The old policeman that brought me here is in the next room, talking to two business guys who came in with me. As far as I know the line between police and mafia here is very thin and vanishing. On the way he casually asked ,,Where did you go to get the extension?”. ,,An agency downtown,” I stammered, to which he laughed because I just said to the chief of the immigration police that I was going to use the bribe channel. Big mistake! I cannot keep forgetting that no matter how public the secret is one is not supposed to act as if it was not a secret. This time it passed with no consequences though: ,,You should rather go straight to the immigration office, “ he said looking aside, “it’s cheaper.” He actually said I should wait outside but I still went in. When they come kick me out I’ll go. They mostly don’t kick me out and that’s how I get places.

So I am sitting in the interrogatory room with two young severely bored policemen. At some point guys leave the room and let their computers unlocked and all their stuff with me only. I get up to check out what’s my position and immediately spot the eye in the sky. CCTV at the top of the tall hall outside. One of the lenses is looking me right in the face. Oh well, I though it would be weird, a police central with no CCTV. I do a leg stretch and sit down. Guys are coming back.

They sit down and start looking into my passport. My knowledge of Bahasa is limited to numbers and bistro menu items but they still use English expressions for things like non-extendable, which actually, my visa is, as the whole scam is based on the police never checking … with the database. I stare at my police officers as hard as if I understood and they begin to look a little intimidated when suddenly the clock strikes midday.

Guys look a bit confused. The database is still down but they feel leaving me there alone and going for lunch would not be a good idea. While, obviously, lunch is the pivotal element of a work day anywhere in the world. Eventually they give me back my passport and ask me to come back in a few hours.

I go get my motorbike out of the parking and leave to come back in the afternoon. There is a random police check in the airport lane again. I indeed don’t have the international driver’s license. This time the traffic police is talking to me but I ignore them and keep going. Why would they go after me. There sure as hell are enough people driving with no papers in line behind me. (In case you didn’t know it, you never pay fine in Indonesia, it is always a bribe.)

One hour later. This time the security bloke at the immigration central is a jolly one. Smiling ear to ear he tries really hard to make it correct. After a while he slowly says “I cannot let you in because this area is for officers only,” and glows with accomplishment. I sit down on the ground. Twenty minutes later I am reading Li Po’s poetry when one of the policemen from before comes to pick me up.

,,The database is still down,” he explains while we are walking through the tall hall . ,,But that’s ok. Give me your passport. I will fix your stamp now.”

I try my best not to look relieved.

Then I turn my face to him to nod off whatever he carries on saying when something behind him catches my attention. We are just passing a set of mirrors in the wall. Mirrors from the outside. The smudges. People were peeping in, not out. It is the office where I sat before. So the interrogation room is the actual CCTV’s blind spot then. With all the passports and expired travel documents and other things that can make a lot of things very much easier in real life, now they are there left lying in some police station, catching dust, forever.

Leaving aside all the scary possibilities of what can be happening in there sometimes with so many random people passing by the mirrors not having a clue, as that is not relevant now: I am a big, big disappointment to myself.

That’s the story of my life. Leaving opportunities on the table because of reasons I only imagine to be true. I should have at least pocketed a pen, just for practice.
