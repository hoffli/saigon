---
layout: blog
title: I would like to
category: blog
tags: [random]  
summary: "kill the freakin moron who broke the mirror"
image: /images/blog/paulie.jpg
---

<style>center{text-align:center}</style>

## Craig "Satoshi" Wright  

The first thing that got me interested was an old TV [interview with CW](https://vimeo.com/149035662).

It just struck me that his hair is freshly trimmed and that he's got a slight rash from the collar of his shirt. This got me speculating that if he pulled this business style regularly these things wouldn't happen as he would have them sorted out without giving them much thought anymore.

I thought, his normal self will be more like... well, like [this](http://www.powerretail.com.au/wp-content/uploads/userphoto/108.jpeg) or [this, some time in the past](https://eforensicsmag.com/wp-content/uploads/2012/07/Photo-for-Bio-247x300.jpg).

Some people on Reddit who had the time to dig deeper did a very good job.

* There is a [spectacular network of companies related to Craig Wright, Dave Kleiman and others](https://www.reddit.com/r/btc/comments/4ifqgn/ira_kleiman_dave_kleimans_brother_has_shares_in_a/) which to me looks somewhat like a form of crony capitalism but on the outsiders' side.

* Others pulled out old mailing lists and diagnosed CW with mental disorders from Wikipedia because he stated he was open and honest with his life and work but at the same time was accused of plagiarism.

* CW has been publishing articles and books since what seems forever but somehow nobody cared. [Here's himself engaging in a debate with a guy who says free market is not a solution to anything](https://www.reddit.com/r/btc/comments/4ieye8/craig_wright_posting_on_a_lulzsec_site_in_2011/). Sad thing is this was quite understandable: Guy is saying free markets are _evil_ (not so strongly worded, _flawed_ more like, actually), that the tragedy is that nothing has intrinsic value because everything is only worthy if enough people consider it worthy and that the people who do the _actual labour_ are paid peanuts unlike the cronies. Guy continues saying, ”how come this is capitalistic - nobody I know _values_ their CEO yet CEO's are paid the most. **Someone** should come up with a better solution, I obviously don't have one.” _Dang, bitch. You need a CEO to be told what to do and you need to hate him to preserve some sense of human individuality in yourself._ Yet, Craig Wright in dialogue with this guy still came across as an utter twat. One would almost say you need a special talent for that.


Funny and meaningless coincidence was that I was reading through all this late at night with my headphones on, performing the drum line of [Linkin Park's Given Up](https://www.youtube.com/watch?v=0xyxtzD54rM) with my chopsticks on my upholstered chair, thinking that if CW was less posh he would have to like Linkin Park.

<blockquote class="twitter-tweet" data-lang="de"><p lang="en" dir="ltr">The leaks Craig Wright (aka Satoshi) was *really* worried about. <a href="https://twitter.com/hashtag/linkinpark?src=hash">#linkinpark</a> <a href="https://twitter.com/hashtag/bitcoin?src=hash">#bitcoin</a> <a href="https://twitter.com/hashtag/commie?src=hash">#commie</a> <a href="https://twitter.com/hashtag/idbescaredtoo?src=hash">#idbescaredtoo</a> <a href="https://t.co/9kRCxtIZx4">pic.twitter.com/9kRCxtIZx4</a></p>&mdash; Jackson Palmer (@ummjackson) <a href="https://twitter.com/ummjackson/status/729207848542986244">8. Mai 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


Then the tweet came to /r/buttcoin. [CW indeed listened to Linkin Park](http://craigswright.tumblr.com/page/4) like all of the disappointed Christian world has been since 2003 or so.

Lines like _I tried so hard and got so far but in the end it doesn't even matter._ The classic of working class.

So, I think I get where CW might come from:

## I would like to kill the freaking moron who broke the mirror

Paulie's opening line from Rocky.

Does it mean he is Satoshi? Does it mean he isn't Satoshi? None. It doesn't mean a thing.

Do I think Bitcoin will change the world? Nah. It will be included in the current system somehow, leveraging the cognitive bias of lifetime outsiders who lead the Bitcoin development ("_less bad_ equals _good_"). Some people will leave lit and at the right time, some people will _feel it in their heart_ etc.

If Craig Wright for some reasons has access to a big stash of coins, I think he'll dump it and gtfo. It might make a nice market crash, in a way he might work against his own profit. I still think it's likely he'll do something like that, if he indeed has the money. It's a massive stash and there is a limit on how much money can be useful in someone's life. You won't make it to have 1 000 000 posh dinners etc. That's why I think to prove a point might have more value to him than money. If he has the stash.

Nonetheless, this CW pursuit got me more into the struggle of under-appreciated losers vs. the ever-happy people who are liked by others since they offer them better _user experience_. All these attempts of digital economies to ”remove humanity from [whatever]”...it all comes from the pain that the human nature of some people is less valuable than the nature of other people - even if these are very similar but in different contexts. In my view it somehow became very tightly related to Bitcoin now. In short, these days were about the time that I, in my thinking, got to the shift from _Bitcoin the safe nation-less store of value_ to _Bitcoin the ocean wave that looks solid and stable for what is a millisecond in the grand scheme of things_, which was meant to finally happen.


> ... life is not fair. It will not ever be fair. There is no such thing as equality. They can never be equality.

> The whole notion of striving for fairness is flawed. There is no universal concept of fair. There is no intrinsic definition of fair. What one person considers fair will always be unfair to another.

> We are not equal and we cannot be equal. As I was pointing out, a student with a 150+ IQ from a poor family cannot be directly compared with a rich student who has a 70 IQ.

>  As one who started in a poor household with a single mother and who paid for his own education, I again say BS.


## ... who broke the mirror

Someone asked me the other day if I don't miss home. I don't. In Asia, human effort and individuality has zero value. I would like to say people won't lie you here about that, telling you to run after _this one last thing_ that will be the deal breaker for you, and that _talent will always win_, or _truth_ and other Bitcoin fanatics' wet dreams like that. But it's probably just me now.

I do get to overhear people talking in my favorite cafe. Even if I purposely choose one that is mostly frequented by locals, not expats, people occur there explaining in condescending tone with southern US accent as loud as it can get that _You must learn better English. You are competing against Indian people who have spoken English since 5 years old. Then we can get you to America. America is a place...Everyone wants to be there._

Understandably I am not keen on working. My landlord keeps saying he is unhappy because of that since whenever someone young lives in the house his family takes them for their children, and it is bad if children are out of work. Of course I don't tell them I sit at the charts. Fucking con bitches would milk the shit out of me. I know very well what I am in Asia: walking ATM.

You see I was there too: Work harder to make up for the fact that you are not worth anything. But what I've done with it was I moved to Asia where knowing I am not worth a shit to anyone works for me.

If I could have children would I tell them that they will never help _themselves_ by purposely working towards a solution? Or that there is no _intrinsic value_ and if you see or _are_ something but sufficient amount of other people ... (Never mind). - Probably not.

I could tell the opposite though - to carry on doing what they liked even if it didn't make sense because coincidence is more powerful than targeted effort anyway. And if it doesn't work out at least they cut their losses.

In fact I think I'll stop talking about the pain, disparity and other sob stories even with myself. Now that my health is gone I get to care more about what I pull through my brain. Somehow, whatever is _right_, _considerate_ or _non-selfish_ ... I cannot help it but it sounds more and more like a total fucking bullshit to me.

[I would like to kill the freakin moron who broke the mirror](/images/blog/toobig.gif)

<iframe width="755" height="480" src="https://www.youtube.com/embed/SBjQ9tuuTJQ" frameborder="0" allowfullscreen></iframe>
