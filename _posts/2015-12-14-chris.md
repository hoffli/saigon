---
layout: blog
title: Chris Ellis on not just made in China
category: blog
tags: [crypto]  
summary: Humans and rag dolls.
image: /images/blog/post1.jpg
---


[A guy called Chris Ellis from bitcoin community makes full nodes from raspberry pis for 12 hours a day making this a reality show footage from a cheapskate couchsurf at his friends’.](https://www.youtube.com/watch?v=atoDsV3eelo)

To show people they can start from scratch and that it is not that scary.

At least that’s what he said before. On the Keiser report above he actually says that it is his art project.

Life of a Chinese factory worker seems interesting to him.

“I wanted to feel their pain,” he says.

<img class="aligncenter" src="https://i.ytimg.com/vi/usB180xcYEY/hqdefault.jpg" alt="Who would you shoot at? Who? - Stalker by A. Tarkovski">

Who would you shoot at? Who? – STALKER by A. Tarkovski

So he has never done it for real. Otherwise he would know that there is no pain. I know – He raises money on it, it must be cool, and it is only cool when you fake it. But what if you cannot fake life like this?

If you work on assembly line for a few weeks you find you got used to it faster than you expected. After all, everyone you know is doing the same.

Then you start to like it.

In the end you can’t imagine your life without it. Do it if you don’t believe it.

It becomes all you know. It gets into your guts.

Try to leave the factory now. Try to change your life from there on – when your lifestyle has gotten into your guts. That’s a fucking achievement because you are going against something that has become your “nature”.

Which brings up the question what is human nature, and self, and if you are at that place in life that I just described, you’ll have to admit that what you thought was your self was in fact imposed on you by a circumstance that stuck around for long enough for you to start perceiving it as your reality.

Is this bad? Should we stop doing it? Can we stop doing it? Or shouldn’t we just choose more carefully what we tolerate in our lives? And if so, based on what should we make our decisions? Based on what should we choose what to exclude from our lives?

We do these decisions every day, by deciding not to go too in depth into something or to be polite to someone and to not be polite to someone else. We are already doing it, but based on what?

How dangerous it is not to take these petty little decisions seriously, when in the long run, they might become marginal gains leading towards something deal breaking? How dangerous it is not to know where we are led?

So what are we, what is personality, and does it make sense to say that it evolves in time? What is this reality that we feel? Is there someone to impose it on us?

Or are we cultivating our reality simply by letting us be part of it?

By letting us be part of it I don’t mean trying it out, I mean letting it spread in our mind and letting it become “us”.

Can it be that there is no “they” – that there is no one to fight against! – that it’s just us, talking about shit instead of keeping our head low and moving on step by step in our own life?

You see this is art and this is science because it generates many questions for which you don’t have immediate answers.

And you should also see there is no reason to consider other people’s lives interesting.

I remember I would walk across cities looking at houses with thousands and thousands of lights feeling thrilled and curious. All the appartments, hotels and offices: What all can be hidden in there?

Now I know.

Life is not interesting and life shouldn’t be interesting. All human lives are the same. We are trying to make good with the deal we got. At the same time we are not doing and not supporting and at any price not justifying whatever we feel pushed to as long as we consider it wrong.

And when I say wrong I don’t mean bad for kids or wrong maybe to some people, you know, this kind of western world wrong. I mean wrong like should-not-exist-on-this-planet.

In this very core way, every human life is the same.

Because anyone who doesn’t live a life like this is not a human but a rag doll.
