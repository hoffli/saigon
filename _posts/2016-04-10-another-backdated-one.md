---
layout: blog
title: Another one
category: blog
tags: [random]  
summary: "bites the dust"
image: /images/future/universe.jpg
---

_26 May 2016. I am backdating this one too. I'm dying. Posts about that will always be bad for user experience._

So it has come to the point of no return. Yesterday I walked to the cafe in the morning and was randomly assesing things I ran into. If I want them in the rest of the time I still have. It was in the street that has the opera house. 

Good coffee. 

Yes. 

Luis Vuitton. 

Bullshit. 

Sirloin steak. 

Yes, but not every day. 

Bitcoin markets.

Absolutely.

The question of identity is unclear and keeps transforming itself. At first it was like hope. Finally I don't have to think ten years ahead and keep loads of money because people spontaneously dislike me and try to scam me. Finally I can make decisions freely. Not like "I" would. Then the reality check came. 

That I will just vanish, leaving nothing behind. People don't even remember my name, and I gradually stopped talking throughout my adult life. 

I wasn't convinced enough about my truth to put pressure on anyone, which made everyone to put pressure on me. There was a lot of impact left upon me but none left by me. Will I make it to change this? Should I? Do I *actually have something* of my own that I can leave behind? Something solid that I can put pressure with? Or has it all been crushed to dust?

I have this foggy idea that it was beyond my powers, that there were really no ways out, nothing that I could possibly think of. I could have run away from home sooner but I know what would happen. All the things that still happened either way, even though when I ran away I was much older. All things that happen when at first people start abusing the fact that you have no family, no place in society of your own, that you have to be at someone's mercy to survive. 

Then as you get hurt enough, the rest of the mankind naturally starts feeling disgust towards you. Then you die. One way or another. It's a rule. It might take a long time but there's no life after all mankind started dreading you.

In a way I am happy it will happen while I am still young. I might make it to the 27 club. People who didn't know me very well might still say that I had everything in front of me, down the line. 

If it came 5 years later, it would already be clear to everyone by then that my life simply failed. Failed life is a very common occurence so no one cares about failed lives. Young life is scarce and has more value. Better to still have something of value when dying.

Today is the day that I realized how rich and full my life could be. That there could be both understanding and people. People who wouldn't mind me in spite of the fact I wouldn't fit in their expectations. That I could still speak. That I would be included in the world. 

But for that you need the world to include you. If it decides you won't be included, then there is nothing you can personally do about it. 

Writing this is like the bloody old freak Seneca, writing letters to an imaginary friend while there was no one in the presently known reality who would lose one word with him.
